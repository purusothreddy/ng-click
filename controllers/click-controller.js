app.controller("clickController", function ($scope) {
    //the $scope is the model. it acts as the glue between the view (HTML) and the controller.
    //Anything specified on the model is accessible in the view.
    //Anything NOT specified on the model is NOT accessible in the view.

    $scope.name = "Ash Ketchum"; // variable name is available in the view.
    var name = "Jane Doe"; // NOT available in the view.

    // defining a function called change name.
    // the function will change the name variable available on the $scope
    // for the function to be available in the view we will define it on the $scope
    $scope.changeName = function(name) {
        // checks if the name has been passed to the function
        // if not, assigns the value Chandler Bing.
        if(name) {
            $scope.name = name;
        } else {
            $scope.name = "Chandler Bing";
        }
    }
})